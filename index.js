let listItems = document.querySelector('.places-table');
let form = document.querySelector('.form-container');
let createLink = document.querySelector('.create');
let backToListLink = document.querySelector('.back-to-list');
let tableEl = document.querySelector('table');
let submitEl = document.querySelector('.flex-container');
let updateEl = document.querySelector('.flex-container-update');

let updateButton = document.querySelector("#update");
let nameInput = document.querySelector("#name");
let ratingInput = document.querySelector("#rating");
let addressInput = document.querySelector("#address");
let imageInput = document.querySelector("#myfile");
let submitButton = document.querySelector("#submit");

let insert = true;
let rowNum = 0;
let savedTable = tableEl.innerHTML;
let reverse = 1;
let places = ["Dhaka", "Faridpur", "Kushtia", "Gazipur", "Gopalganj", "Kishoreganj", "Madaripur",
    "Manikganj", "Munshiganj", "Narayanganj", "Narshingdi", "Rajbari", "Shariatput", "Tangail", "Bandarban",
    "Brahmanbaria", "Chandpur", "Chattogram", "Cox's bazar", "Cumilla", "Feni", "Khagrachhori", "Lakshmipur",
    "Noakhali", "Rangamati", "Habiganj", "Moulvibazar", "Sunamganj", "Sylhet", "Barguna", "Barishal", "Vola",
    "Jhalokati", "Patuakhali", "Pirojpur", "Bagerhat", "Chuadanga","Khulna"];


createLink.addEventListener("click", toggleSection);
backToListLink.addEventListener("click", toggleSection);
tableEl.addEventListener("click", deleteRow);
tableEl.addEventListener("click", updateRow);
updateButton.addEventListener("click", updateRowBtn);
submitButton.addEventListener("click", addRow);



function createTable() {
    tableEl.innerHTML = savedTable;

}


function toggleSection() {
    //console.log("span clicked");
    if (form.style.display === "none") {
        listItems.style.display = "none";
        form.style.display = "block";
    }

    else {
        listItems.style.display = "block";
        form.style.display = "none";
    }
}

function toggleInsert() {

    if (!insert) {
        submitEl.style.display = "flex";
        updateEl.style.display = "none";
    }
    else {
        submitEl.style.display = "none";
        updateEl.style.display = "flex";
    }
    insert = !insert;

}

function deleteRow(e) {
    if (!e.target.classList.contains('delete')) {
        return;
    }
    const btn = e.target;
    btn.closest('tr').remove();
}

function updateRow(e) {
    if (!e.target.classList.contains('update')) {
        return;
    }

    const btn = e.target;
    let rowEl = btn.closest('tr');
    rowNum = rowEl.rowIndex;
    toggleSection();
    toggleInsert();
}



function updateRowBtn(e) {
    let template = getRowTemplate(e);
    tableEl.rows[rowNum].innerHTML = template;
    toggleInsert();
    e.preventDefault();
}

function addRow(e) {
    e.preventDefault();
    let template = getRowTemplate(e);


    tableEl.tBodies[0].innerHTML += template;
    savedTable = tableEl.innerHTML;

    
}


function getRowTemplate(e) {
    let name = nameInput.value;
    let rating = ratingInput.value;
    let address = addressInput.value;
    let image;
    try {
        image = URL.createObjectURL(imageInput.files[0]);
    } catch (error) {
        alert("Image upload is not valid.");
    }
    
    let isValid = false;
    if(rating>=1 && rating<=5 && name.length != 0 && address.length != 0 && image.length != 0)
    {
        isValid = true;
    }
    let template=``;

    if (isValid) 
    {
        template = `
        <tr>
          <td>${name}</td>
          <td>${address}</td>
          <td>${rating}</td>
          <td><img src=${image} alt="Beach image"></td>
          <td>
            <a href="#"><span class="update">UPDATE</span></a>
            <a href="#"><span class="delete">DELETE</span></a>
            
          </td>
        </tr>
    `;
        toggleSection();
    }
    else
    {
        if(rating<0 || rating>5)
        {
            alert("Invalid Input! The rating must be in between 1 to 5.");
        }
        else
        {
            alert("Invalid Input! Please fill up all the fields.");
        }
        
    }


    return template;

}


function sortTable() {

    let tb = tableEl.tBodies[0], tr = Array.prototype.slice.call(tb.rows, 0), i;
    reverse = -((+reverse) || -1);
    console.log(tb);
    console.log(tableEl);

    tr = tr.sort(function (a, b) {
        return reverse
            * (a.cells[2].textContent.trim()
                .localeCompare(b.cells[2].textContent.trim())
            );
    });
    for (i = 0; i < tr.length; ++i) tb.appendChild(tr[i]);
}


function autocomplete(inp, arr)
{
    let currentFocus;
    inp.addEventListener("input", function (e) {
        let a, b, i, val = this.value;
        closeAllLists();
        if (!val) { return false; }
        currentFocus = -1;
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        this.parentNode.appendChild(a);
        for (i = 0; i < arr.length; i++) {
            if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                b = document.createElement("DIV");
                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                b.addEventListener("click", function (e) {
                    inp.value = this.getElementsByTagName("input")[0].value;
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    inp.addEventListener("keydown", function (e) {
        let x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            currentFocus++;
            addActive(x);
        } else if (e.keyCode == 38) {
            currentFocus--;
            addActive(x);
        } else if (e.keyCode == 13) {
            e.preventDefault();
            if (currentFocus > -1) {
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        if (!x) return false;
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        for (let i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        let x = document.getElementsByClassName("autocomplete-items");
        for (let i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}


autocomplete(document.getElementById("address"), places);